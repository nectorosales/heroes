import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {SnackBarService} from '../services/shared/snackbar.service';
import {Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AuthHelperService {

  constructor(
    private http: HttpClient,
    private snackService: SnackBarService,
    private translate: TranslateService
  ) {}

  runHttpGet(url: string): Observable<any> {
    return this.http
      .get(`${url}`)
      .pipe( map(data => {
        return data;
      }));
  }

  runHttpStore<T>(url: string, obj, showAlert = true): Observable<any> {
    return this.http
      .post<T>(`${url}`, obj)
      .pipe(
        map(data => {
          if (showAlert) {
            this.snackService.showAlert(this.translate.instant('shared.snackbar.store'));
          }
          return data;
        })
      );
  }

  runHttpUpdate(url: string, obj, showAlert = true): Observable<any> {
    return this.http
      .put(`${url}`, obj)
      .pipe(
        map(data => {
          if (showAlert) {
            this.snackService.showAlert(this.translate.instant('shared.snackbar.update'));
          }
          return data;
        })
      );
  }

  runHttpDestroy(url: string, obj, showAlert = true): Observable<any> {
    return this.http
      .delete(`${url}`)
      .pipe(
        map(data => {
          if (showAlert) {
            this.snackService.showAlert(this.translate.instant('shared.snackbar.destroy'));
          }
          return data;
        })
      );
  }


}
