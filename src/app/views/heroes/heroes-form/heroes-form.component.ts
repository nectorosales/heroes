import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Heroe from '../../../interfaces/Heroe';
import {MatDialogRef} from '@angular/material/dialog';
import {DialogComponent} from '../../../shared/dialog/dialog.component';
import {TitleCasePipe} from '@angular/common';

@Component({
  selector: 'app-heroes-form',
  templateUrl: './heroes-form.component.html',
  styleUrls: ['./heroes-form.component.css'],
  providers: [TitleCasePipe]
})
export class HeroesFormComponent implements OnInit {

  angForm: FormGroup;
  @Input('someProp') someProp = null;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<DialogComponent>, public titleCasePipe: TitleCasePipe) {
    this.createForm();
  }

  createForm(): void {
    this.angForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      createdAt: [''],
      updatedAt: [''],
    });
  }

  getHeroe(heroe: Heroe): void {
    this.angForm.get('id').setValue(heroe.id);
    this.angForm.get('name').setValue(heroe.name);
    this.angForm.get('createdAt').setValue(heroe.createdAt);
    this.angForm.get('updatedAt').setValue(heroe.updatedAt);
  }

  ngOnInit(): void {
    if (this.someProp) {
      this.getHeroe(this.someProp);
    }
  }

  do(): boolean {
    this.angForm.get('name').setValue(this.titleCasePipe.transform(this.angForm.get('name').value));
    if (this.angForm.valid) {
      this.dialogRef.close({
        data: this.angForm.value
      });
      return true;
    }
  }
}
