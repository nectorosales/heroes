import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {HeroesService} from '../../../services/heroes.service';
import Heroe from '../../../interfaces/Heroe';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DialogComponent} from '../../../shared/dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {HeroesFormComponent} from '../heroes-form/heroes-form.component';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css'],
  providers: [
    DatePipe
  ],
})
export class HeroesListComponent implements OnInit, AfterViewInit {

  public activeLang = 'es';
  displayedColumns: string[] = ['id', 'name', 'createdAt', 'updatedAt', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource<Heroe>([]);

  constructor(private heroesService: HeroesService, private dialog: MatDialog, private translate: TranslateService) {
    this.translate.setDefaultLang(this.activeLang);
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroesService.index().subscribe((heroes: Heroe[]) => {
      this.dataSource = new MatTableDataSource(heroes);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openForm(heroe: Heroe = null): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: heroe ? this.translate.instant('heroe.edit-heroe') : this.translate.instant('heroe.new-heroe'),
        component: HeroesFormComponent,
        data: heroe,
        buttons_disabled: true
      },
      disableClose: true,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (heroe) {
          result.data.updatedAt = new Date();
          this.heroesService.update(result.data).subscribe( () => {
            this.getHeroes();
          });
        } else {
          result.data.createdAt = new Date();
          result.data.updatedAt = new Date();
          this.heroesService.store(result.data).subscribe( () => {
            this.getHeroes();
          });
        }
      }
    });
  }

  destroy(heroe: Heroe): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: { title: this.translate.instant('shared.attention'), msgDialog: this.translate.instant('shared.attention-info')},
      disableClose: true,
      autoFocus: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.heroesService.destroy(heroe).subscribe( () => {
          this.getHeroes();
        });
      }
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public changeLanguage(lang): void {
    this.activeLang = lang;
    this.translate.use(lang);
  }

}
