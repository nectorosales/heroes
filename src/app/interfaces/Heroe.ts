export default class Heroe {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}
