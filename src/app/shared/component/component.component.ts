import {
  AfterViewInit,
  ChangeDetectorRef,
  Compiler,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

@Component({
  selector: 'app-component',
  templateUrl: './component.component.html',
  styleUrls: ['./component.component.css']
})
export class ComponentComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('target', { read: ViewContainerRef }) target;
  @Input() component;
  @Input() data;
  cmpRef: ComponentRef<any>;
  private isViewInitialized = false;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private compiler: Compiler,
    private cdRef: ChangeDetectorRef
  ) {}

  updateComponent(): void {
    if (!this.isViewInitialized) {
      return;
    }
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }

    const factory = this.componentFactoryResolver.resolveComponentFactory(
      this.component
    );
    this.cmpRef = this.target.createComponent(factory);
    this.cmpRef.instance.someProp = this.data;
    this.cdRef.detectChanges();
  }

  ngOnChanges(): void {
    this.updateComponent();
  }

  ngAfterViewInit(): void{
    this.isViewInitialized = true;
    this.updateComponent();
  }

  ngOnDestroy(): void {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }

  ngOnInit(): void {}
}
