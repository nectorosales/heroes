import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar, private translate: TranslateService) {}

  showAlert(message: string, ngClass: string | string[] = ['bg-dark']): void {
    this.snackBar.open(message, this.translate.instant('shared.snackbar.close'), {
      duration: 3500,
      panelClass: ngClass
    });
  }

}
