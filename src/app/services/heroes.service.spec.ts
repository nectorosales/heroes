import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HeroesService } from './heroes.service';
import Heroe from '../interfaces/Heroe';
import {HttpClient} from '@angular/common/http';
import {AuthHelperService} from '../helpers/auth-helper.service';
import {SnackBarService} from './shared/snackbar.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Overlay} from '@angular/cdk/overlay';

describe('HeroesService', () => {
  let service: HeroesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HeroesService, HttpClient, AuthHelperService, SnackBarService, MatSnackBar, Overlay ]
    });
    service = TestBed.inject(HeroesService);
    httpMock = TestBed.inject(HttpTestingController);
  });


  it('find', () => {
    const heroe: Heroe = {
      "id": 2,
      "name": "Batman",
      "createdAt": "1968-11-16T00:00:00",
      "updatedAt": "1968-11-16T00:00:00"
    };

    service.find(2).subscribe(h => {
      expect(h).toBe(heroe);
    });

    const request = httpMock.expectOne(`${service.uri}` + '/2');

    expect(request.request.method).toBe('GET');

    request.flush(heroe);

  });

});
