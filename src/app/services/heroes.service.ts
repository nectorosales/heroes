import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {AuthHelperService} from '../helpers/auth-helper.service';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  uri = environment.api + '/heroes';

  constructor(private authHelperService: AuthHelperService) { }

  index(): Observable<any> {
    return this.authHelperService.runHttpGet(this.uri);
  }

  find(id): Observable<any> {
    return this.authHelperService.runHttpGet(this.uri + '/' + id);
  }

  store(obj): Observable<any> {
    return this.authHelperService.runHttpStore(this.uri, obj);
  }

  update(obj): Observable<any> {
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, obj);
  }

  destroy(obj): Observable<any> {
    return this.authHelperService.runHttpDestroy(this.uri + '/' + obj.id, obj);
  }
}
